(define-page index "We already are web!"
  (:h1 :class "doesnt-actually-look-like-a-header-but-whatever"
       "Are we"
       (:em "web")
       "yet?")
  (:h1 :class "actually-the-header"
       "We already are web!")
  (:main
   (:p "Common Lisp has had HTTP servers since"
       (:a :href "https://archive.is/6sY0E" "1994,")
       "and today has many"
       (:a :href "frameworks.html" "frameworks")
       "and"
       (:a :href "libraries.html" "libraries")
       "that simplify web development. Paired with Common
Lisp's"
       (:a :href "tools.html" "dynamic and introspective tools,")
       "developing a web program is very rapid and efficient.")
   (:p "Many requirements of web programs can be handled using plain Common
Lisp, such as the "
       (:a :href "http://marijnhaverbeke.nl/postmodern/" "Postmodern")
       "ORM, but some use the"
       (:a :href "https://common-lisp.net/project/cffi/" "Common Foreign Function Interface,")
       "such as the"
       (:a :href "https://common-lisp.net/project/cl-plus-ssl/" "CL+SSL")
       "SSL library.")

   (:h2 "Can I replace my Node/Django/Iron now?")
   (:p (:strong "Absolutely!")
       "Almost everything provided in popular frameworks can be found in Lisp, and
tools like"
       (:a :href "https://www.quicklisp.org/beta/" "Quicklisp")
       "install dependencies automagically, minimizing deployment issues. You may find some
problems are better solved in Lisp, as macros can handle resource acquisition and release
and can drastically reduce boilerplate.")
   
   (:h2 "Getting started")
   (:p "You should install a few components as well as a Common Lisp implementation
to get started with web development in Common Lisp."
       (:strong "A very convenient shortcut is to install a portable system such as"
                (:a :href "https://portacle.github.io/" "Portacle")
                "to manage all the parts that will be installed; but if you have an
operating system that can update these components (such as GNU/Linux), you may be
better served by installing them yourself. That's your call.")
       (:ul
        (macrolet ((named-entry (name &body text)
                     `(htm (:li (:strong ,(concatenate 'string name ":")) ,@text))))
          (named-entry "Compilers" "Common Lisp has very fast compilers, which are
often competitive with C while retaining runtime safety and even some dynamic
typing."
                       (:a :href "https://sbcl.org" "SBCL")
                       "and"
                       (:a :href "https://ccl.clozure.com/" "Clozure CL")
                       "are two popular compilers; SBCL produces marginally faster
machine code but is somewhat slower to compile than CCL. Some users, such as"
                       (:a :href "https://tech.grammarly.com/blog/posts/Running-Lisp-in-Production.html"
                           "Grammarly")
                       "use CCL in development environments and SBCL in production.")
          (named-entry "Editors"
                       (:a :href "https://www.gnu.org/software/emacs/"
                           "GNU Emacs,")
                       "along with"
                       (:a :href "https://common-lisp.net/project/slime/"
                           "SLIME,")
                       "is the usual recommendation for a Lisp editor, but other solutions
exist, such as "
                       (:a :href "https://kovisoft.bitbucket.io/tutorial.html"
                           "SLIMV")
                       "for Vim. Clozure CL also comes with an editor in its Mac App Store bundle, named"
                       (:a :href "https://itunes.apple.com/us/app/clozure-cl/id489900618" "Hemlock.")
                       "Any editor with basic Lisp support should do, but specialized editors and
extensions will provide you with the most powerful tools for debugging and inspection.")
          (named-entry "Libraries"
                       (:a :href "libraries.html" "We maintain a list of some good libraries here.")))))))
