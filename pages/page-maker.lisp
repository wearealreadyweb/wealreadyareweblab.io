(ql:quickload '(:cl-who))
(use-package '(:cl-who))
(setf (html-mode) :html5)

(defmacro define-page (page-name title &body body)
  `(with-open-file (*standard-output* ,(format nil "~(~a~).html" page-name)
                                      :direction :output
                                      :if-exists :supersede)
     (with-html-output (*standard-output* nil :prologue t :indent t)
       (:html
        (:head
         (:title ,title)
         (:meta :name "viewport" :content "width=device-width, initial-scale=1")
         (:link :rel "stylesheet" :type "text/css" :href "style/style.css"))
        (:body
         ,@body
         (:footer
          (:p (:b "We Already Are Web")
              "|"
              (:a :href "https://gitlab.com/wearealreadyweb/wealreadyareweb.gitlab.io"
                  "GitLab"))))))))
